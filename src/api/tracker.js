import axios from "axios";
import { AsyncStorage } from "react-native";

const Instance = axios.create({
  baseURL: "http://a0c72d3e5ff2.ngrok.io",
});

Instance.interceptors.request.use(
  async (config) => {
    const token = await AsyncStorage.getItem("token");
    if (token) {
      config.headers.Authorization = `Bearer ${token}`;
    }
    return config;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export default Instance;
